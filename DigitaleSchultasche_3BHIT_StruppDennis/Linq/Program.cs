﻿using System;
using System.Linq;
using ScottLINQ;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            //int x = 23;
            //int? y = 44;
            //y = null;
            //y = 89;



            Scott_Schema scott = new Scott_Schema();
            //var uebungSÜ1 = from d in scott.dept
            //              where d.deptno >= 20
            //              select d;

            //foreach (var item in uebungSÜ1)
            //{
            //    Console.WriteLine($"{item.dname} {item.loc}");
            //}



            //var uebungSÜ2 = from d in scott.dept
            //              where d.deptno >= 20
            //              orderby d.loc
            //              select new { d.dname, ort = d.loc };

            //foreach (var item in uebungSÜ2)
            //{
            //    Console.WriteLine(item);
            //}




            //// uebung 1
            //var uebung1 = from d in scott.dept
            //              select d;

            //foreach (var item in uebung1)
            //{
            //    Console.WriteLine(item);
            //}


            //// uebung 2

            //var uebung2 = from d in scott.emp
            //              where d.deptno == 10
            //              select new { d.ename, d.job, d.hiredate };
            //foreach (var item in uebung2)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 3
            //var uebung3 = from d in scott.emp
            //              where d.job == "Clerk"
            //              select new { d.ename, d.job, d.hiredate };
            //foreach (var item in uebung3)
            //     {
            //    Console.WriteLine(item);
            //     }

            ////uebung 4
            //var uebung4 = from d in scott.emp
            //          where d.deptno != 10
            //          select new { d.ename};
            //foreach (var item in uebung4)
            //     {
            //    Console.WriteLine(item);
            //     }

            ////uebung 5
            //var uebung5 = from d in scott.emp
            //              where d.comm > d.sal
            //              select new { d.ename };
            //foreach (var item in uebung5)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 6
            //var uebung6 = from d in scott.emp
            //              where d.hiredate == new DateTime(1980, 12, 03)
            //              select new { d.ename };
            //foreach (var item in uebung6)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 7 
            //var uebung7 = from d in scott.emp
            //              where d.sal < 1250 && d.sal > 1600
            //              select new { d.ename, d.sal };
            //foreach (var item in uebung7)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 8

            //var uebung8 = from d in scott.emp
            //              where d.job != "MANAGER" && d.job != "PRESIDENT"
            //              select new { d.ename};
            //foreach (var item in uebung8)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 9
            //var uebung9 = from d in scott.emp
            //              where d.ename[3] == 'a'
            //              select new { d.ename };
            //foreach (var item in uebung9)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 10

            //var uebung10 = from d in scott.emp
            //              where d.comm != null
            //              select new { d.ename, d.empno, d.job };
            //foreach (var item in uebung10)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 11

            //var uebung11 = from d in scott.emp
            //               orderby d.comm
            //               select new { d.empno, d.ename, d.job, d.mgr, d.hiredate, d.sal, d.comm, d.deptno };


            //foreach (var item in uebung11)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 12

            //var uebung12 = from d in scott.emp
            //           where !(d.job == "MANAGER" | d.job == "PRESIDENT")
            //           orderby d.deptno, d.hiredate descending
            //           select new { d.empno, d.ename, d.job, d.mgr, d.hiredate, d.sal, d.comm, d.deptno };

            //foreach (var item in uebung12)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 13

            //var uebung13 = from d in scott.emp
            //               where d.ename.Length == 6
            //               select new {d.ename};

            //foreach (var item in uebung13)
            //{
            //    Console.WriteLine(item);
            //}


            ////uebung 14

            //var uebung14 = from d in scott.emp
            //               where d.deptno == 30
            //               select new { d.ename, d.job };

            //foreach (var item in uebung14)
            //{
            //    Console.WriteLine(item.ename + "-" + item.job);
            //}

            ////uebung 16

            //int tage = 22;
            //int stunden = 8;

            //var uebung16 = from d in scott.emp
            //           where d.deptno == 30
            //           select new { d.ename, MONATLICH = d.sal, TÄGLICH = d.sal / tage, STÜNDLICH = (d.sal / tage) /stunden };

            //foreach (var item in uebung16)
            //{
            //    Console.WriteLine(item);
            //}

            ////uebung 17
            //var uebung17 = from d in scott.emp
            //           select new { d.sal };

            //double all_sal = 0;
            //foreach (var item in uebung17)
            //{
            //    all_sal += item.sal;
            //}
            //Console.WriteLine(all_sal);







            //Angaben  ÜBUNGSZETTEL 2       

            //uebung 19

            //var uebung19 = (from d in scott.emp
            //               where d.deptno == 30 && d.sal > 0
            //               select new { d.ename }).Count();
            //Console.WriteLine(uebung19);

            //var uebung19_2 = (from d in scott.emp
            //                where d.deptno == 30 && d.comm > 0
            //                select new { d.ename }).Count();
            //Console.WriteLine(uebung19_2);


            //uebung 20

            //var uebung20 = (from d in scott.emp
            //                select new { d.job }).Distinct().Count();
            //Console.WriteLine(uebung20);


            //uebung 21
            //beim ersten alle einzeln unterschiedlichen  ausgegeben 


            // beim zweiten das sie zusammen unterschiedlich gecountet

            //uebung 22
            //var uebung22 = (from d in scott.emp
            //                where d.deptno == 30
            //                select d.comm ?? 0);
            //Console.WriteLine(uebung22);


            //uebung 23
            //var uebung23 =  from d in scott.emp
            //                where d.job != "MANAGER" && d.job != "PRESIDENT"
            //                group d by d.deptno into b

            //                from details in (from e2 in b
            //                                 group e2 by e2.job)

            //                group details by b.Key;

            //foreach (var item in uebung23.OrderBy (x => x.Key))
            //{
            //    Console.WriteLine(item.Key + "");
            //    foreach (var detail in item)
            //    {
            //        Console.WriteLine(detail.Key + "");
            //    }
            //    Console.WriteLine();
            //}




            //uebung 24              
            //var uebung24 = from d in scott.emp
            //               group d by d.deptno into b
            //               select b.Count();

            //Console.WriteLine(uebung24.Average());


            //uebung 25

            //var uebung25 = from d in scott.emp
            //               where d.job == "MANAGER" || d.job == "PRESIDENT"
            //               select new { d.deptno, d.hiredate, d.ename, d.comm, d.empno, d.job, d.mgr, d.sal };

            //foreach (var item in uebung25)
            //{
            //    Console.WriteLine(item);
            //}


            //uebung 26

            //var uebung26 = from d in scott.emp
            //               where d.comm > (d.sal * 0.25)
            //               select new { d.ename, d.job, d.comm };

            //foreach (var item in uebung26)
            //{
            //    Console.WriteLine(item);
            //}


            //uebung 27

            //var uebung27 = (from d in scott.emp 
            //                select d).Min
            //                (x => x.comm + x.sal);

            //Console.WriteLine(uebung27);


            //uebung 28

            //var uebung28 = (from d in scott.emp
            //                select d).Min
            //                (x => x.hiredate);

            //Console.WriteLine(uebung28);

            //uebung 29

            var uebung29 = from d in scott.emp
                           group d by d.deptno into b
                           from details in (from e2 in b
                                            group e2 by e2.job)
                           group details by b.Key;
                          

            foreach (var item in uebung29.OrderBy(x => x.Key))
            {
                Console.WriteLine(item.Key + "");
                foreach (var detail in item)
                {
                    Console.WriteLine(detail.Key + "");
                    Console.WriteLine(detail.Count());
                }
                Console.WriteLine();
            }


        }


    }
            




        
}
    

